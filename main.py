# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

# Find the largest palindrome made from the product of two 3-digit numbers.

palindrome = -1
a=-1
b=-1
range = range(999,99, -1)
for i in range:
    for j in range:
        product = i*j
        if (str(product) == str(product)[::-1] and product > palindrome):
            palindrome = product
            a=i
            b=j
print(f"a={a} b={b} a*b={palindrome}")